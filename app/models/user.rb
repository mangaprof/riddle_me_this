class User < ActiveRecord::Base
  has_many :riddles, dependent: :destroy
  validates :name, presence: true, uniqueness: true
  has_secure_password
  validates :password, presence: true, length: { minimum: 8, maximum: 32 }
end
