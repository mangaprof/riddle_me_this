class Riddle < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  validates :question, presence: true
  validates :answer_1, presence: true
  validates :answer_2, presence: true
  validates :answer_3, presence: true
  validates :answer_4, presence: true
  validates :right_answer, inclusion: { in: 1..4 }
end
