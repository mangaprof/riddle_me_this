module RiddlesHelper
  def set_current_riddle(riddle)
    session[:riddle_id] = riddle.id
  end
  
  def current_riddle
    Riddle.find_by(id: session[:riddle_id])
  end
end
