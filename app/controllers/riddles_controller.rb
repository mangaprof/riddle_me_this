class RiddlesController < ApplicationController
  before_action :logged_in_user

  def new
    @newRiddle = current_user.riddles.build
    @errorMsg = ""
    @successMsg = ""
  end

  def create
    @newRiddle = current_user.riddles.build(riddle_params)
    if @newRiddle.save
      @errorMsg = ""
      @successMsg = "Riddle successfully received. Do another."
      @newRiddle = current_user.riddles.build
    end
    render "new"
  end

  def show
    get_riddle
  end

  def answer
    if params[:answers].to_i == current_riddle.right_answer
      @errorMsg = ""
      @successMsg = "Correct!"
    else
      @errorMsg = "That was incorrect. Perhaps you should read more."
      @successMsg = ""
    end
    get_riddle
    render "show"
  end

  private

    def logged_in_user
      unless logged_in?
        redirect_to login_url
      end
    end

    def get_riddle
      rand_id = rand(Riddle.count + 1)
      @riddle = Riddle.where("id >= ?", rand_id).limit(1).order("id asc")[0]
      set_current_riddle(@riddle)
    end

    def riddle_params
      params.require(:riddle).permit(:question, :answer_1, :answer_2, :answer_3, :answer_4, :right_answer)
    end
end
