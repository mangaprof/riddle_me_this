class UsersController < ApplicationController
  def account
    @newUser = User.new
    @errorMsg = ''
  end

  def create
    @newUser = User.new(user_params)
    if @newUser.save
      log_in @newUser
      redirect_to '/'
    else
      render 'account'
    end
  end

  def login
    user = User.find_by(name: params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to '/'
    else
      @newUser = User.new
      @errorMsg = 'Invalid email/password combination'
      render 'account'
    end
  end

  def logout
    log_out
    redirect_to '/'
  end

  private

    def user_params
      params.require(:user).permit(:name, :password, :password_confirmation)
    end
end
