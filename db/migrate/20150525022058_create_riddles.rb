class CreateRiddles < ActiveRecord::Migration
  def change
    create_table :riddles do |t|
      t.text :question
      t.references :user, index: true
      t.string :answer_1
      t.string :answer_2
      t.string :answer_3
      t.string :answer_4
      t.integer :right_answer

      t.timestamps null: false
    end
    add_foreign_key :riddles, :users
  end
end
