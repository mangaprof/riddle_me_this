Rails.application.routes.draw do
  root 'static_pages#home'

  get 'account' => 'users#account'
  post 'login' => 'users#login'
  delete 'logout' => 'users#logout'
  resources :users

  get 'answer_riddle' => 'riddles#show'
  post 'answer_riddle' => 'riddles#answer'
  get 'new_riddle' => 'riddles#new'
  post 'new_riddle' => 'riddles#create'
  resources :riddles
end
